import sys
import re
import os
import json
import time
import subprocess
import dbus
from PyQt5 import QtCore, QtDBus

class AutoResolution():

    def __init__(self):
        self.item = "org.kde.KScreen"
        self.path = "/backend"
        self.interface = "org.kde.kscreen.Backend"
        self.logFilePath = "/tmp/logresolution.txt"
        self.supportedDisplayModeList = []
        self.supportedDisplayModeList.clear()
        #self.check_if_target_reached() - does not currently work
        self.get_x_supported_modes()

    def escape_ansi(self, line):
        ansi_escape = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -/]*[@-~]')
        return ansi_escape.sub('', str(line))

    def check_if_target_reached(self):
        logFile = open('/tmp/resolutionTarget.txt', 'a')
        try:
            target_service = subprocess.check_output(["systemctl", "status", "graphical.target"])
            target_output = target_service.decode("utf-8")
        except subprocess.CalledProcessError as e:
            target_output = e.output.decode("utf-8")

        target_out_split = target_output.split("\n")
        target_extract_active = target_out_split[2].split(" ")
        target_isActive = target_extract_active[4]
        if target_isActive == "active":
            print("Got Active Target", file=logFile)
            logFile.close()
            self.get_session_type()
        else:
            print("Active Target Not Reached", file=logFile)
            time.sleep(5)
            self.check_if_target_reached()

    def get_session_type(self):
        logFile2 = open('/tmp/sessionType.txt', 'a')
        print("In Get Session Type", file=logFile2)
        try:
            sessionType = subprocess.check_output(["echo", "-n", os.environ["XDG_SESSION_TYPE"]]).decode("utf-8")
        except subprocess.CalledProcessError as e:
            print(e.output.decode("utf-8"), file=logFile2)
        print(sessionType, file=logFile2)
        if sessionType == "wayland":
            print("found wayland", file=logFile2)
            self.get_wayland_supported_modes()
        else:
            print("found x", file=logFile2)
            self.get_x_supported_modes()

    def get_session_type_xrandr(self):
        # Try fetching session type even earlier than graphical target
        get_session = subprocess.check_output(["xrandr"])
        get_output = get_session.decode("utf-8")
        get_output_string = get_output.split("\n")
        get_output_string_split = get_output_string[2].split(" ")
        sessionType = get_output_string_split[0]
        if sessionType == "XWAYLAND0":
            print("found wayland")
            self.get_wayland_supported_modes()
        else:
            print("found x")
            self.get_x_supported_modes()

    def get_session_type_dbus(self):
        # Requires watch on dbus-org.freedesktop.login1 service
        bus = dbus.SystemBus()
        sesObj = bus.get_object('org.freedesktop.login1', '/org/freedesktop/login1/session/self')
        iface = dbus.Interface(sesObj, dbus_interface="org.freedesktop.DBus.Properties")
        iface_m = iface.get_dbus_method("Get", dbus_interface=None)
        sessionType = iface_m("org.freedesktop.login1.Session", "Type")
        print(sessionType)
        if sessionType == "wayland":
            print("found wayland")
            self.get_wayland_supported_modes()
        else:
            print("found x")
            self.get_x_supported_modes()

    def get_x_supported_modes(self):
        print("In Get X Supported Modes")
        try:
            bus = QtDBus.QDBusConnection.sessionBus()
            kscreenObject = QtDBus.QDBusInterface(self.item, self.path, self.interface, bus)
            results = kscreenObject.call("getConfig").arguments()
            for x in results[0]['outputs']:
                if x['type'] == 6.0:
                    displayID = results[0]['outputs'][0]['id']
                    displayWidth = results[0]['outputs'][0]['size']['width']
                    displayHeight = results[0]['outputs'][0]['size']['height']
                    displayPrefMode = results[0]['outputs'][0]['preferredModes']
                    displayModes = results[0]['outputs'][0]['modes']
                    for modes in displayModes:
                        if modes['size']['width'] == 1920 and modes['size']['height'] == 1080:
                            self.supportedDisplayModeList.append(modes)
                    self.set_display_size(displayID, displayWidth, displayHeight)

        except Exception as e:
            print("Dbus not avaialable")
            get_display_id = os.popen("kscreen-doctor -o | grep '*' | awk '{print $2}'").read().splitlines()[0]
            display_id = int(self.escape_ansi(get_display_id))
            list_connected = os.popen("kscreen-doctor -o | grep 'primary'")
            connected_devices = list_connected.read().splitlines()[0]
            connected_devices_get_mode = connected_devices.partition('Modes:')[2]
            cleaned_unwanted_chars = connected_devices_get_mode.partition('Geometry')[0]
            get_modes_items =  cleaned_unwanted_chars.split(' ')
            mode_list = []
            displaymodes = {}
            supportedAppendList = []
            for items in get_modes_items:
                if items != '':
                    item_filter = self.escape_ansi(items)
                    if item_filter != '':
                        mode_list.append(self.escape_ansi(item_filter))

            for items in mode_list:
                split_to_get_mode = items.split(':')
                id = split_to_get_mode[0]
                width = split_to_get_mode[1].split('x')[0]
                height_break = split_to_get_mode[1].split('x')[1]
                height = height_break.split('@')[0]
                refresh_rate = height_break.split('@')[1]
                refresh_rate = refresh_rate.replace('@', '')
                if '*' in refresh_rate:
                    height = height.replace('*', '')
                if '!' in refresh_rate:
                    height = height.replace('!', '')

                # find if height has a character
                if '*' in split_to_get_mode[1]:
                    currentId = split_to_get_mode[0]
                    currentWidth = split_to_get_mode[1].split('x')[0]
                    implicitHeight = split_to_get_mode[1].split('x')[1]
                    currentHeight = implicitHeight.split('@')[0]

                supportedAppendList.append({'id': int(id), 'name': '', 'refreshRate': refresh_rate, 'size': {'width': int(width), 'height': int(height)}})

            displayModes = supportedAppendList
            for modes in displayModes:
                if modes['size']['width'] == 1920 and modes['size']['height'] == 1080:
                    self.supportedDisplayModeList.append(modes)
            self.set_display_size(display_id, currentWidth, currentHeight)

    def get_wayland_supported_modes(self):
        logFileWayland = open('/tmp/setWaylandResolution.txt', 'a')
        print("In Get Wayland Supported Modes", file=logFileWayland)
        try:
            waylandSessionObject = subprocess.check_output(["kscreen-doctor", "-o", "-j"]).decode("utf-8")
            print(waylandSessionObject, file=logFileWayland)
        except subprocess.CalledProcessError as e:
            print(e.output, file=logFileWayland)
        begin, end = waylandSessionObject.find('{'), waylandSessionObject.rfind('}')
        filtered_waylandSessionObject = waylandSessionObject[begin: end+1]
        print(filtered_waylandSessionObject, file=logFileWayland)
        results = json.loads(filtered_waylandSessionObject)
        for x in results['outputs']:
            if x['type'] == 6.0:
               displayID = results['outputs'][0]['id']
               displayWidth = results['screen']['currentSize']['width']
               displayHeight = results['screen']['currentSize']['height']
               displayModes = results['outputs'][0]['modes']
               for modes in displayModes:
                   if modes['size']['width'] == 1920 and modes['size']['height'] == 1080:
                       self.supportedDisplayModeList.append(modes)
                       print(modes, file=logFileWayland)
               self.set_display_size(displayID, displayWidth, displayHeight)

    def set_display_size(self, displayId, displayWidth, displayHeight):
        if int(displayHeight) > 1080 and int(displayWidth) > 1920:
           setDisplayMode = self.supportedDisplayModeList[0]['id']
           setDisplayId = int(displayId)
           setDisplayArgs = "output.{0}.mode.{1}".format(setDisplayId, setDisplayMode)
           setDisplayConfig = subprocess.call(["kscreen-doctor", setDisplayArgs])
           bus = dbus.SessionBus()
           remote_object = bus.get_object("org.kde.bigscreen", "/Plugin")
           remote_object.autoResolutionChanged(dbus_interface = "org.kde.bigscreen")

autoResolution = AutoResolution()
